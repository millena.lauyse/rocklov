describe "GET /equipos/{equipo_id}" do
  before(:all) do
    # primeiro precisa fazer o login e recuperar o user_id para ser enviado no cabeçalho da requisição para rota /equipos
    payload = { email: "to@mate.com", password: "pwd123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "Obter unico equipo" do
    before(:all) do
      # Dado que eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumb("sanfona.jpg"),
        name: "Sanfona",
        category: "Outros",
        price: 499,
      }
      #remove o equipamento cadastrado do banco danco de dados, para poder cadastrar novamente
      MongoDB.new.remove_equipo(@payload[:name], @user_id)

      # E eu tenho o id do equipamento
      @equipo = Equipos.new.create(@payload, @user_id)

      # a veriavel euqipo_id recebe apenas o id do response json
      @equipo_id = @equipo.parsed_response["_id"]

      # Quando faço uma requisição get por id
      @result = Equipos.new.find_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end

    it "deve retornar o nome" do
      # essa linha busca o nome do equipamento (name) para verificar se o nome vai ser retornado mesmo ao buscar o equipamento por id
      expect(@result.parsed_response).to include("name" => @payload[:name])
    end
  end

  context "equipo nao existe" do
    before(:all) do
      # passa por paramentro um object id aleatório para retornar erro 404
      @result = Equipos.new.find_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 404" do
      expect(@result.code).to eql 404
    end
  end
end

describe "GET /equipos" do
  before(:all) do
    # primeiro precisa fazer o login e recuperar o user_id para ser enviado no cabeçalho da requisição para rota /equipos
    payload = { email: "penelope@gmail.com", password: "pwd123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "Obter uma lista" do
    before(:all) do
      # Dado que eu tenho uma lista de equipos
      payloads = [
        {
          thumbnail: Helpers::get_thumb("conga.jpg"),
          name: "Conga",
          category: "Outros",
          price: 499,
        },
        {
          thumbnail: Helpers::get_thumb("trompete.jpg"),
          name: "Trompete",
          category: "Outros",
          price: 599,
        },
        {
          thumbnail: Helpers::get_thumb("slash.jpg"),
          name: "Les Paul do Slash",
          category: "Outros",
          price: 699,
        },
      ]

      payloads.each do |payload|
        MongoDB.new.remove_equipo(payload[:name], @user_id)
        Equipos.new.create(payload, @user_id)
      end

      # QUANDO faço uma requisição GET para /equipos
      @result = Equipos.new.list(@user_id)
    end

    it "Dever retornar 200" do
      expect(@result.code).to eql 200
    end

    it "Deve retornar uma lista de equipos" do
      expect(@result.parsed_response).not_to be_empty
      # puts @result.parsed_response
      # puts @result.parsed_response.class
    end
  end
end
