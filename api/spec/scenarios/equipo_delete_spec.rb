#encoding: utf-8
describe "DELETE /equipos/{equipo_id}" do
  before(:all) do
    # primeiro precisa fazer o login e recuperar o user_id para ser enviado no cabeçalho da requisição para rota /equipos
    payload = { email: "to@mate.com", password: "pwd123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "Deletar unico equipo" do
    before(:all) do
      # Dado que eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumb("pedais.jpg"),
        name: "Pedais",
        category: "Áudio e Tecnologia".force_encoding("ASCII-8BIT"),
        price: 199,
      }
      #remove o equipamento cadastrado do banco danco de dados, para poder cadastrar novamente
      MongoDB.new.remove_equipo(@payload[:name], @user_id)

      # E eu tenho o id do equipamento
      @equipo = Equipos.new.create(@payload, @user_id)

      # a veriavel euqipo_id recebe apenas o id do response json
      @equipo_id = @equipo.parsed_response["_id"]

      # Quando faço uma requisição DELETE por id
      @result = Equipos.new.remove_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end

  context "equipo nao existe" do
    before(:all) do
      # passa por paramentro um object id aleatório para retornar erro 404
      @result = Equipos.new.remove_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end
end
