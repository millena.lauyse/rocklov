#describe cria uma suite de testes
describe "POST /sessions" do
  context "login com sucesso" do
    #before(:all) executa uma unica vez antes de todos os cenários
    before(:all) do
      payload = { email: "betao@hotmail.com", password: "pwd123" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end

    # parsed_response é o response parseado para o padrão do ruby, que é o hash
    # puts result.parsed_response
    #  puts result.parsed_response.class
  end

  # é um array de massas em modo hash, que vai ser convertido em json
  #   examples = [
  #     {
  #       title: "senha incorreta",
  #       payload: { email: "millena@yahoo.com", password: "12345" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },

  #     {
  #       title: "usuario nao existe",
  #       payload: { email: "404@yahoo.com", password: "12345" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "email em branco",
  #       payload: { email: "", password: "12345" },
  #       code: 412,
  #       error: "required email",
  #     },

  #     {
  #       title: "sem o campo email",
  #       payload: { password: "12345" },
  #       code: 412,
  #       error: "required email",
  #     },

  #     {
  #       title: "senha em branco",
  #       payload: { email: "millena@yahoo.com", password: "" },
  #       code: 412,
  #       error: "required password",
  #     },
  #     {
  #       title: "sem o campo senha",
  #       payload: { email: "millena@yahoo.com" },
  #       code: 412,
  #       error: "required password",
  #     },
  #   ]

  # carregando o arquivo de massa de dados 'login.yml' dentro da variavel 'examples' atraves do metodo get_fixtures
  examples = Helpers::get_fixture("login")

  #foreach que percorre o array de massa 'examples' e executa o teste em cada iteração do loop
  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida a mensagem" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
