require "httparty"

# Essa classe possui a url padrão e os recursos do httparty, para que outras classes herdem e evite e repetição de codigo.
class BaseApi
  include HTTParty
  base_uri "http://rocklov-api:3333"
end
