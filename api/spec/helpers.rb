module Helpers
  #esse método obtem uma massa de testes em arquivo da pasta fixtures
  def get_fixture(item)
    YAML.load(File.read(Dir.pwd + "/spec/fixtures/#{item}.yml"), symbolize_names: true)
  end

  # esse metodo encapsula a chaad apara abrir arquivo de imagem
  def get_thumb(file_name)
    # esse metodo retorna o diretório para abrir a imagem e depois passar na requisição da api
    # o argumento 'rb' grava o aqruivo no formato binario
    return File.open(File.join(Dir.pwd, "spec/fixtures/images", file_name), "rb")
  end

  module_function :get_fixture
  module_function :get_thumb
end
