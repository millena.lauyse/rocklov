# importando as classes no require_relative neste aruquivo de hlpers, não será mais necessário fazer os imports nos
# aqruivos que precisam dessas classes para funcionar
require_relative "routes/signup"
require_relative "routes/sessions"
require_relative "routes/equipos"

require_relative "libs/mongo"
require_relative "helpers"

require "digest/md5"

# a senha no banco de dados é criptografada do tipo MD5, chamando o modulo md5 o ruby cosegue criptografar string para md5
# o metodo criado abaixo "to_md5" faz a conversão de uma string para md5
def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  # gancho (hooks) executado uma única vez para todos os testes automatizados
  # aqui é criado uma semente de massa de testes e o array users será cadastrado no banco de dados
  config.before(:suite) do
    users = [
      { name: "Roberto Silva", email: "betao@hotmail.com", password: to_md5("pwd123") },
      { name: "Tomate", email: "to@mate.com", password: to_md5("pwd123") },
      { name: "Penelope", email: "penelope@gmail.com", password: to_md5("pwd123") },
      { name: "Joe Perry", email: "joe@gmail.com", password: to_md5("pwd123") },
      { name: "Edward Cullen", email: "ed@gmail.com", password: to_md5("pwd123") },
    ]

    MongoDB.new.drop_danger
    MongoDB.new.insert_users(users)
  end
end
