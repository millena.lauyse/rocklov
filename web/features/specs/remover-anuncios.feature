#language: pt

Funcionalidade: Remover Anuncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anuncio
    Para que eu possa manter meu Dashboard atualizado

    Contexto: Login
        * Login com "spider@bol.com" e "pwd123"
    
    Cenario: Remover um anuncio
        Dado que eu tenho um anuncio indesejado:
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 50             |
        Quando eu solicito a exclusao desse item
            E confirmo a exclusao
        Então nao devo ver esse item no meu Dashboard

    @temp
    Cenario: Desistir da exclusao
        Dado que eu tenho um anuncio indesejado:
            | thumb     | violino.jpg |
            | nome      | Violino     |
            | categoria | Cordas      |
            | preco     | 100         |
        Quando eu solicito a exclusao desse item
            Mas nao confirmo a solicitacao
        Então esse item deve permanecer no meu Dashboard