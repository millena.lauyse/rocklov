Dado("Login com {string} e {string}") do |email, password|
  @email = email

  @login_page.open
  @login_page.login(email, password)

  #Checkpoint para garantir que só vai sair do step quando estiver no dashboard
  expect(@dash_page.on_dash?).to be true
end

Dado("que acesso o formulario de cadastro de anuncios") do
  sleep 3
  @dash_page.goto_equipo_form

  # aqui temos um checkpoint = ponto de verificação para garantir que está de fato na pagina de cadastro de anuncios, não é uma validação
  # expect(page).to have_css "#equipoForm"  -> linha migrada para a classe Equipos Page
end

Dado("que eu tenho o seguinte equipamento:") do |table|
  # rows_hash converte uma tabela de chave valor para um objeto ruby
  # criando variavel com '@' a variavelé criada como global, e dá pra usar durante toda a execução
  @anuncio = table.rows_hash

  #acessa o banco para excluir o anuncio já criado para evitar erro
  MongoDB.new.remove_equipo(@anuncio[:nome], @email)
end

Quando("submeto o cadastro desse item") do
  @equipos_page.create(@anuncio)
end

Então("devo ver esse item no meu Dashboard") do
  # valida que o anuncio criado tem o nome da imagem que foi passada como massa de dados, é um recurso rspec
  expect(@dash_page.equipo_list).to have_content @anuncio[:nome]
  expect(@dash_page.equipo_list).to have_content "R$#{@anuncio[:preco]}/dia"
end

Então("deve conter a mensagem de alerta: {string}") do |expected_alert|
  # o meetodo have_text é tipo um contém, ou seja busca se contem o texto dentro do elemento, e não se sao iguais
  expect(@alert.dark).to have_text expected_alert
end

# Remover Anuncios
Dado("que eu tenho um anuncio indesejado:") do |table|
  # Vamos usar a api para fazer o cadastro do anuncio indesejado
  # para isso o capybara vai recuperar por javascrips o user_id pelo localStorage da aplicação web
  user_id = page.execute_script("return localStorage.getItem('user')")

  # a variavel thumbail recebe o diretório para abrir a imagem e depois passar na requisição da api
  # o argumento 'rb' grava o aqruivo no formato binario
  thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumb]), "rb")

  @equipo = {
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
  }
  # aqui a consome a api de cadastrar equipos
  EquiposService.new.create_equipo(@equipo, user_id)

  # faz um refresh na pagina, como se fosse um F5
  visit current_path
end

Quando("eu solicito a exclusao desse item") do
  @dash_page.request_removal(@equipo[:name])
end

Quando("confirmo a exclusao") do
  @dash_page.confirm_removal
end

Então("nao devo ver esse item no meu Dashboard") do
  # o metodo has_no_equipo? foi criado como pageObject para retornar true ou false
  expect(@dash_page.has_no_equipo?(@equipo[:name])).to be true
end

Quando("nao confirmo a solicitacao") do
  @dash_page.cancel_removal
end

Então("esse item deve permanecer no meu Dashboard") do
  # valida que o anuncio criado tem o nome da imagem que foi passada como massa de dados, é um recurso rspec
  expect(@dash_page.equipo_list).to have_content @equipo[:name]
end
