Dado("que meu perfil de anunciante é {string} e {string}") do |email, password|
  @email_anunciante = email
  @pass_anunciante = password
end

Dado("que eu tenho o seguinte equipamento cadastrado:") do |table|
  # Vamos usar a api para fazer o cadastro do anuncio indesejado, para isso  criamos a classe SessionsService
  #  e dentro da classe tem o metodo get_user_id que faz a chamada de login json retorna o id do usuario
  user_id = SessionsService.new.get_user_id(@email_anunciante, @pass_anunciante)

  # a variavel thumbail recebe o diretório para abrir a imagem e depois passar na requisição da api
  # o argumento 'rb' grava o aqruivo no formato binario
  thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumb]), "rb")

  @equipo = {
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
  }
  MongoDB.new.remove_equipo(@equipo[:name], @email_anunciante)

  # aqui a consome a api de cadastrar equipos
  result = EquiposService.new.create_equipo(@equipo, user_id)
  @equipo_id = result.parsed_response["_id"]
  log @equipo_id
end

Dado("acesso meu dashboard") do
  #fazer login para acessar o dashboard

  @login_page.open
  @login_page.login(@email_anunciante, @pass_anunciante)

  #Checkpoint para garantir que só vai sair do step quando estiver no dashboard
  expect(@dash_page.on_dash?).to be true
end

Quando("{string} e {string} solicita a locacao desse equipo") do |email, password|
  user_id = SessionsService.new.get_user_id(email, password)
  EquiposService.new.booking(@equipo_id, user_id)
end

Então("devo ver a seguinte mensagem:") do |doc_string|
  # o metodo 'gsub' é um recurso ruby pra fazer substring, ou seja vai substituir "DATA_ATUAL"  pela data do sistema obtida
  # pelo comando 'Time.strftime("%d/%m/%Y")'
  expected_message = doc_string.gsub("DATA_ATUAL", Time.now.strftime("%d/%m/%Y"))
  expect(@dash_page.order).to have_text expected_message
end

Então("devo ver os links: {string} e {string} no pedido") do |button_accept, button_reject|
  # espero que na pagina contenha o css selector com texto passado por parametro
  # expect(page).to have_selector ".notifications button", text: button_accept
  # expect(page).to have_selector ".notifications button", text: button_reject

  # a mesma coisa que as linhas de cima, porem agora usando page_objects
  expect(@dash_page.order_actions(button_accept)).to be true
  expect(@dash_page.order_actions(button_reject)).to be true
  sleep 30
end
