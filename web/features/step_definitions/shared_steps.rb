Então('sou redirecionado para o Dashboard') do
    #o comando "expect" é um recurso de validação do pacote rspec, "page" é um objeto do capybara 
    expect(@dash_page.on_dash?).to be true
end

Então('vejo a mensagem de alerta: {string}') do |expected_alert|
    # busca o elemento por css selector, usando a classe que é represtnada pelo pontp ( . ) e adiciona na variavel alert
    # alert = find(".alert-dark")

    #usando por pageobjects
    expect(@alert.dark).to eql expected_alert
end