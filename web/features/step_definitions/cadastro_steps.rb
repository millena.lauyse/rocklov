# dentro dos steps definitions serão usados códigos do capybara para automatizar o comportamento descrito no bdd
Dado("que acesso a página de cadastro") do
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable

  #  log table.hashes
  # variavel user recebe a tabela da feature. o metodo hashes converte para o padrão do ruby em forma de array, como nesse arry só tem uma linha, o ocmando .first recupera a primeira posição do array
  user = table.hashes.first

  #  log user

  #instancia a classe MongoDB para acessar o método de rmoção do registro no banco de dados
  MongoDB.new.remove_user(user[:email])
  @signup_page.create(user)
end
