require "allure-cucumber"
require "capybara"
require "capybara/cucumber"
require "faker"
require "webdrivers"

# CONFIG é uma constante que vai receber o caminho de configuração de ambiente.
# o conando ENV é um recurso do ruby para conseguir acesso as variaveis de ambiente que estão na pasta config
# e carrega o ambient que está no arquivo cucumber.yml, que no nosso caso é o ambiente local
# essa constante pode ser usada em qualquer lugar do projeto
CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))

case ENV["BROWSER"]
when "firefox"
  @driver = :selenium
when "chrome"
  @driver = :selenium_chrome
when "fire_headless"
  @driver = :fire_headless
when "chrome_headless"
  @driver = :selenium_chrome_headless
else
  #raise estoura uma exceção e aborta o processo mostrando a mensagem
  raise "Navegador Incorreto, variavel @driver está vazia :("
end

# aqui configura o navegador padrão que será google crhome
Capybara.configure do |config|
  config.default_driver = @driver

  #configuração da url padrão, assim em outros lugares passamos apenas a rota
  config.app_host = CONFIG["url"]
  config.default_max_wait_time = 10
end

AllureCucumber.configure do |config|
  config.results_directory = "/logs"
  config.clean_results_directory = true
end
