Before do
  # assim não precisa instanciar o pageobject da classe loginPage nos arquivos .rb
  #pageobjects com apps actions
  @login_page = LoginPage.new
  @alert = Alert.new
  @signup_page = SignupPage.new
  @dash_page = DashPage.new
  @equipos_page = EquiposPage.new

  # maximizar a janela: page é objeto do capypara
  # driver é para ter acesso ao selenium
  # page.driver.browser.manage.window.maximize

  # outra forma é definir um tamanho padrão de janela
  page.current_window.resize_to(1440, 900)
end

After do
  temp_shot = page.save_screenshot("logs/temp_screenshot.png")
  Allure.add_attachment(
    name: "Screenshot",
    type: Allure::ContentType::PNG,
    source: File.open(temp_shot),
  )
end
