class LoginPage
  # esse include é um recurso parecido com herança: isso fala para o ruby que a classe LoginPage precisa conhecer todos os recursos do capybara
  include Capybara::DSL

  def open
    visit "/"
  end

  def login(email, password)
    find("input[placeholder='Seu email']").set email
    find("input[type=password]").set password
    click_button "Entrar"
  end
end
