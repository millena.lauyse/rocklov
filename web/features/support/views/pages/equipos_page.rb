class EquiposPage
  include Capybara::DSL

  def create(equipo)
    # o metodo has_css? retorna um true ou false, nesse caso, essa linha substitui o expect que não pode ser usado nessa classe
    # pode ser chamado de chckpoint com timeout explicito (pq usa o timeout do capybara)
    page.has_css?("#equipoForm")

    # só vai executar o método upload se o conteudo (nome da imagem) dele for > 0, ou seja se não for vazio
    upload(equipo[:thumb]) if equipo[:thumb].length > 0

    # o '$' é uma tecnica de expressao regular para procurar em um input uma string que termina com o texto 'equipamento'
    find("input[placeholder$=equipamento]").set equipo[:nome]

    select_cat(equipo[:categoria]) if equipo[:categoria].length > 0

    #o '^' é uma expressão regular para encontrar o elemento com o texto que inicia com a palavra Valor
    find("input[placeholder^=Valor]").set equipo[:preco]

    click_button "Cadastrar"
    sleep 3
  end

  def select_cat(cat)
    #primeiro encontra o campo select pelo id #category, depois dentro do select procura entre as options com texto passado na tabela de entrada de dados e o selciona pelo select_option
    find("#category").find("option", text: cat).select_option
  end

  def upload(file_name)
    # Dir.pwd é recurso do ruby que faz com recupere o diretorio do projeto, que é a pasta rocklov
    thumb = Dir.pwd + "/features/support/fixtures/images/" + file_name

    # o comando visible:false é um recurso capybara que faz procurar no html mesmo que esteja oculto na página
    find("#thumbnail input[type=file]", visible: false).set thumb
  end
end
