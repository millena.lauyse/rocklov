class SignupPage
  include Capybara::DSL

  def open
    visit "/signup"
  end

  def create(user)

    # para buscar elemento com id por css selector, é usado o hashtag
    # o comando user[:nome] recupera da tabela o conteúdo do nme informando na feature, assim como emil e senha
    find("#fullName").set user[:nome]
    find("#email").set user[:email]
    find("#password").set user[:senha]

    click_button "Cadastrar"
  end
end
