class DashPage
  include Capybara::DSL

  #quando criamos um método com '?' ele automaticamente já retorna true ou false
  def on_dash?
    page.has_css?(".dashboard")
  end

  def goto_equipo_form
    click_button "Criar anúncio"
  end

  def equipo_list
    return find(".equipo-list")
  end

  def has_no_equipo?(name)
    # o metodo has_no_css? retorna true ou false. O capybara pergunta se na pagina Nâo contem um css com essa combinaçao
    # passada por argumento
    return page.has_no_css?(".equipo-list li", text: name)
  end

  def request_removal(name)
    # faz a busca pela classe da lista de equipamentos e junta com o nome do equipo para fazer a busca por nome
    equipo = find(".equipo-list li", text: name)
    equipo.find(".delete-icon").click
  end

  def confirm_removal
    click_on "Sim"
  end

  def cancel_removal
    click_on "Não"
  end

  def order
    return find(".notifications p")
  end

  def order_actions(name)
    return page.has_css?(".notifications button", text: name)
  end
end
