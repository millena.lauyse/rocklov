require "mongo"

# salva os registros de log ndo mongo em um arquivo, ao invés de mostrar no console
Mongo::Logger.logger = Logger.new("./logs/mongo.log")

class MongoDB
  attr_accessor :client, :users, :equipos

  #construtor da classe MongoDB
  def initialize
    # faz a conexão com banco de dados mongo e guarda a conexão na variavel client
    @client = Mongo::Client.new(CONFIG["mongo"])

    # coleção users no banco de dados, é como se fosse a tabela de usuarios se estivesse usando sql, a mesma coisa para equipos
    @users = client[:users]
    @equipos = client[:equipos]
  end

  def drop_danger
    @client.database.drop
  end

  def insert_users(docs)
    @users.insert_many(docs)
  end

  def remove_user(email)
    #exlcui o documento (registro) do banco de dados com base no email
    users.delete_many({ email: email })
  end

  def get_user(email)
    user = users.find({ email: email }).first
    return user[:_id]
  end

  def remove_equipo(name, email)
    user_id = get_user(email)
    equipos.delete_many({ name: name, user: user_id })
  end
end
